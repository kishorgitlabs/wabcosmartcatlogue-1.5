package search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import addcart.CartDAO;
import addcart.CartDTO;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class SearchActivity extends Activity {
    private ImageView vehicleBackImageView;
	private ImageView Cart_Icon;
	private SharedPreferences myshare;
	private SharedPreferences.Editor edit;
	private String UserType;
    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


		myshare = getSharedPreferences("registration", MODE_PRIVATE);
		edit = myshare.edit();

        this.vehicleBackImageView = (ImageView) findViewById(R.id.back);
		Cart_Icon = (ImageView) findViewById(R.id.cart_icon);


		UserType = myshare.getString("usertype", "").toString();
		if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
			Cart_Icon.setVisibility(View.VISIBLE);
		else
			Cart_Icon.setVisibility(View.GONE);

		Cart_Icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				CartDAO cartDAO = new CartDAO(getApplicationContext());
				CartDTO cartDTO = cartDAO.GetCartItems();
				if(cartDTO.getPartCodeList() == null)
				{
					StyleableToast st =
							new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
					st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
					st.setTextColor(Color.WHITE);
					st.setMaxAlpha();
					st.show();
				}else
				{
					startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
				}
				//  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
			}
		});

        this.vehicleBackImageView.setOnClickListener(new OnClickListener() {
        	 public void onClick(View arg0) {
                 SearchActivity.this.onBackPressed();
             }
		});


        
        ((LinearLayout) findViewById(R.id.assemply_search)).setOnClickListener(new OnClickListener() {
			
        	public void onClick(View v) {
                Intent search = new Intent(SearchActivity.this, Search_Part_Activity.class);
                SearchActivity.this.startActivity(search);
            }
		});
        ((LinearLayout) findViewById(R.id.service_search)).setOnClickListener(new OnClickListener() {
			
        	public void onClick(View v) {
                Intent search = new Intent(SearchActivity.this, ServiceOrRepairActivity.class);
                search.putExtra("Search_type", "Service Part No");
                search.putExtra("Search_types", "Service \nParts No");
                search.putExtra("Search_title", " Service Parts Search");
				search.putExtra("from", "service");
                SearchActivity.this.startActivity(search);
            }
		});
        ((LinearLayout) findViewById(R.id.Repair_search)).setOnClickListener(new OnClickListener() {
			
        	public void onClick(View v) {
                Intent search = new Intent(SearchActivity.this, ServiceOrRepairActivity.class);
                search.putExtra("Search_type", "Repair Kit No");
                search.putExtra("Search_types", "Repair \nKits No");
                search.putExtra("Search_title", "Repair Kits Search");
				search.putExtra("from", "repair");
                SearchActivity.this.startActivity(search);
            }
		});
        ((LinearLayout) findViewById(R.id.Description)).setOnClickListener(new OnClickListener() {
			
        	public void onClick(View v) {
                SearchActivity.this.startActivity(new Intent(SearchActivity.this, Description_search.class));
            }
		});
        ((LinearLayout) findViewById(R.id.Performance_search)).setOnClickListener(new OnClickListener() {
			
        	public void onClick(View v) {
                SearchActivity.this.startActivity(new Intent(SearchActivity.this, PEkit_SearchActivity.class));

            }
		});

		((LinearLayout) findViewById(R.id.equivalent_search)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				SearchActivity.this.startActivity(new Intent(SearchActivity.this, EquivalentActivity.class));
			}
		});



		final ImageView menu = (ImageView)findViewById(R.id.menu);
		menu.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Context wrapper = new ContextThemeWrapper(SearchActivity.this, R.style.PopupMenu);
				final PopupMenu pop = new PopupMenu(wrapper, v);
				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
							case R.id.home:
								startActivity(new Intent(SearchActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
								break;
						case R.id.search:
							startActivity(new Intent(SearchActivity.this, SearchActivity.class));
							break;
						case R.id.notification:
							startActivity(new Intent(SearchActivity.this, NotificationActivity.class));
							break;
						case R.id.vehicle:
							startActivity(new Intent(SearchActivity.this, VehicleMakeActivity.class));
							break;
						case R.id.product:
							startActivity(new Intent(SearchActivity.this, ProductFamilyActivity.class));
							break;
						case R.id.performance:
							startActivity(new Intent(SearchActivity.this, PE_Kit_Activity.class));
							break;
							case R.id.contact:
								startActivity(new Intent(SearchActivity.this, Network_Activity.class));
								break;
							case R.id.askwabco:
								startActivity(new Intent(SearchActivity.this, AskWabcoActivity.class));
								break;
							case R.id.pricelist:
								startActivity(new Intent(SearchActivity.this, PriceListActivity.class));
								break;
						case R.id.update:
							WabcoUpdate update = new WabcoUpdate(SearchActivity.this);
							update.checkVersion();
							break;
						}
						return false;
					}
				});
				pop.setOnDismissListener(new OnDismissListener() {

					@Override
					public void onDismiss(PopupMenu arg0) {
						// TODO Auto-generated method stub
						pop.dismiss();
					}
				});

				pop.inflate(R.menu.main);
				pop.show();
			}
		});


    }
    
   
}
