package directory;

import android.os.Environment;
import android.os.StatFs;

public class CheckExternalMemory {
	public double getExternalMemorySize() {
		if (!Environment.getExternalStorageState().equals("mounted")) {
			return 0.0d;
		}
		StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
		statFs.restat(Environment.getExternalStorageDirectory().getPath());
		return ((double) ((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()))
				/ 4)) / 1024.0d;
	}
}
