package home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Timer;

import alertbox.Alertbox;
import network.NetworkConnection;
import registration.RegisterActivity;
import registration.ServerConnection;

public class HomePage extends Activity {
	int noofsize = 5;
	int count = 0;
	Timer timer;
	private SharedPreferences myshare;
	private Editor edit;
	public ProgressDialog loadDialog;
	public Connection connection;
	public Statement stmt;
	private String android_id;
	private int versionCode;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		myshare = getSharedPreferences("registration", MODE_PRIVATE);
		edit = myshare.edit();
		android_id = Secure.getString(getContentResolver(),
                Secure.ANDROID_ID);

		PackageManager pm = getPackageManager();
		PackageInfo pi = null;
		try {
			pi = pm.getPackageInfo(getPackageName(), 0);
			versionCode = pi.versionCode;
			Log.v("Version name",Integer.toString(versionCode));
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		edit.putInt("VERSION", versionCode).commit();

		if(!myshare.getBoolean("isregistration", false))
		{
				//CheckInternetForID();
			
				/*Intent reg = new Intent(HomePage.this,HomePage.class);
				reg.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(reg);*/
			Intent reg = new Intent(HomePage.this, RegisterActivity.class);
			//reg.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(reg);
			
		}
		else
		{
			setContentView(R.layout.main);

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Intent mainIntent = new Intent(HomePage.this,
							MainActivity.class);
					mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(mainIntent);
				}
			}, 3000);
		}

	}
	protected void CheckInternetForID() {
		// TODO Auto-generated method stub
		NetworkConnection isnet = new NetworkConnection(HomePage.this);
		if(isnet.CheckInternet())
		{
			
			new IsRegister().execute();
			
		}
		else {
			showAlert("No Internet Connection Please Check Your Connection");
		}

	}
	
	
	
	
	public class IsRegister extends AsyncTask<String, Void, String>
	{


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}


		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try
			{

				ServerConnection server = new ServerConnection();
				connection = server.getConnection();
				stmt = connection.createStatement();
				Log.v("Query", "select deviceid from registration where  deviceid ='"+android_id+"' ");
				ResultSet rset =stmt.executeQuery("select deviceid from registration where  deviceid ='"+android_id+"'");

				if(rset.next())
				{
					connection.close();
					stmt.close();
					return "registered";
				}
				else
				{
					connection.close();
					stmt.close();
					return "unregister";
				}

			}
			catch(Exception e)
			{
				return "notsuccess";

			}



		}



		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result.equals("registered"))
			{
				
				edit.putBoolean("isregistration", true);
				edit.commit();

				Intent go = new Intent(HomePage.this,HomePage.class);
				go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(go);
				
			}
			else if(result.equals("unregister"))
			{
			
				Intent reg = new Intent(HomePage.this,RegisterActivity.class);
				reg.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(reg);
				
				
			}
			
		}
	}

	

	
	private void showAlert(String string) {
		// TODO Auto-generated method stub
		Alertbox alert = new Alertbox(HomePage.this);
		alert.showAlertbox(string);
	}
	
}





