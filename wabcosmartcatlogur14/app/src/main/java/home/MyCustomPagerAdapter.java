
package home;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyCustomPagerAdapter extends PagerAdapter {

    private List<Integer> homeImageList;
    LayoutInflater layoutInflater;
    Context context;
    String[] cityResourseList;
    String[] hotelNameResourseList;
    private int position;

    public MyCustomPagerAdapter(Context context) {
        this.context = context;

        // Image Resources
        homeImageList = new ArrayList<Integer>();
        Integer[] webServicesImageResourcesList = {R.drawable.album1,
                R.drawable.album2, R.drawable.album3};
        homeImageList = Arrays.asList(webServicesImageResourcesList);

    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return homeImageList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.viewpager_item,container, false);

        ImageView image = (ImageView) view.findViewById(R.id.imageView);
        image.setImageResource(homeImageList.get(position));
        image.setTag(position);
        container.addView(view);

        return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}

