package registration;

import android.Manifest;
import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import fcm.Config;
import home.HomePage;
import models.asc.ASCDetails;
import models.usermodel.User;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RegisterActivity extends Activity {


    private static final int REQUEST_WRITE_STORAGE = 112;
    private static final int PERMISSION_REQUEST = 100;
    private static final String GST_PATTERN = "\\d{2}[A-Z]{5}\\d{4}[A-Z]{1}\\d[Z]{1}[A-Z\\d]{1}";
    private static final String PAN_PATTERN = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()
            );
    Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    public static Retrofit retrofit = builder.build();
    public ProgressDialog loadDialog;
    public Connection connection;
    public Statement stmt;
    public AlertDialog alertDialog;
    // boolean ismailexits = false;
    public ResultSet rset;
    String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
            .getAbsolutePath().toString() + "/mysdfile.txt";
    Account[] accounts;
    Pattern emailPattern;
    boolean ismailexits = false;
    String namestring, phonestring, emailstring, citystring, countrystring, currentDate, pincodestring, dealercodestring;
    private EditText name;
    private EditText phone;
    private EditText email, dealercode, gstnumber, pannumber;
    private MaterialSpinner state, usertype;
    private EditText city, country, Shop, Address, pincode;
    private Button okay;
    private Button cancel;
    private SharedPreferences myshare;
    private Editor edit;
    private String finalmail;
    private FileInputStream fileInputStream;
    private int STORAGE_PERMISSION_CODE = 23;
    private String IME;
    private String wabcoNumber = "9840864750";
    private String SelectedState, SelectedUser = "";
    private ArrayAdapter<CharSequence> adapter;
    private ArrayList<String> usertypeList;
    private String[] drawerResourcesList;
    private List<String> stateList;
    private String output = "", UserID = "";
    private Alertbox box = new Alertbox(RegisterActivity.this);
    private LinearLayout AddressLayout;
    private String ShopNameString, AddressString;
    private User user;
    private int SelectedStatePosition, SelectedUserPosition;
    private Pattern pattern;
    private Matcher matcher;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        setContentView(R.layout.activity_register);
        name = (EditText) findViewById(R.id.nameedit);
        phone = (EditText) findViewById(R.id.phoneedit);
        email = (EditText) findViewById(R.id.emailedit);
        state = (MaterialSpinner) findViewById(R.id.stateedit);
        city = (EditText) findViewById(R.id.cityedit);
        pincode = (EditText) findViewById(R.id.pincodeedit);
        country = (EditText) findViewById(R.id.countryedit);
        gstnumber = (EditText) findViewById(R.id.gstedit);
        dealercode = (EditText) findViewById(R.id.delaercode_edit);
        pannumber = (EditText) findViewById(R.id.panedit);

        usertype = (MaterialSpinner) findViewById(R.id.usertypeedit);

        AddressLayout = (LinearLayout) findViewById(R.id.address_layout);
        Shop = (EditText) findViewById(R.id.shopedit);
        Address = (EditText) findViewById(R.id.addressedit);

        okay = (Button) findViewById(R.id.okay_register);
        cancel = (Button) findViewById(R.id.cancel);


        okay.setText("Register");

        usertypeList = new ArrayList<String>();
        usertypeList.add("Select Usertype");
        usertypeList.add("Whole Sale Distributor");
        usertypeList.add("Dealer");
        usertypeList.add("OEM Dealer");
        usertypeList.add("Garage Mechanic");
        usertypeList.add("Fleet Operator");
        usertypeList.add("Wabco employee");
        usertypeList.add("ASC");
        usertypeList.add("Others");
        usertypeList.add("FSR");

        state.setBackgroundResource(R.drawable.autotextback);
        usertype.setBackgroundResource(R.drawable.autotextback);

        /*
         * adapter = ArrayAdapter.createFromResource(this, R.array.state_array,
         * R.layout.simple_spinner_item);
         * adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         * state.setAdapter(adapter);
         */

        drawerResourcesList = getResources().getStringArray(R.array.state_array);
        stateList = new ArrayList<String>();
        stateList = Arrays.asList(drawerResourcesList);
        state.setItems(stateList);
        state.setPadding(30, 0, 0, 0);

        usertype.setItems(usertypeList);
        usertype.setPadding(30, 0, 0, 0);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        currentDate = df.format(c.getTime());

        state.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                SelectedState = item.toString();
                Log.v("State", SelectedState);
                SelectedStatePosition = position;
                // Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });

        usertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                SelectedUser = item.toString();
                Log.v("State", SelectedUser);
                SelectedUserPosition = position;
                if (SelectedUser.equals("ASC")) {
                    dealercode.setHint("ASC Code");
                    AddressLayout.setVisibility(View.VISIBLE);
                } else {
                    dealercode.setHint("Dealer Code");
                    AddressLayout.setVisibility(View.GONE);
                }
                if (SelectedUser.equals("Dealer") || SelectedUser.equals("OEM Dealer")) {
                    AddressLayout.setVisibility(View.VISIBLE);
                } else {
                    AddressLayout.setVisibility(View.GONE);
                }
                // Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });

        dealercode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (SelectedUser.equals("ASC") && dealercode.length() == 6) {
                    getAscdetails();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        okay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (usertype.getText().toString().equals("Select Usertype")) {
                    StyleableToast st = new StyleableToast(RegisterActivity.this, "Select your usertype !",
                            Toast.LENGTH_SHORT);
                    st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if (name.getText().toString().equals("")) {
                    name.setError("Enter your Name");
                    StyleableToast st =
                            new StyleableToast(RegisterActivity.this, "Enter your Name !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if (phone.getText().toString().length() != 10) {
                    phone.setError("Enter Your MobileNumber");
                    StyleableToast st = new StyleableToast(RegisterActivity.this,
                            "Enter Your MobileNumber !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    if (email.getText().length() == 0) {
                        email.setError("Enter your Email id");
                        StyleableToast st = new StyleableToast(RegisterActivity.this, "Enter Your Email id !", Toast.LENGTH_SHORT);
                        st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                        st.setTextColor(Color.WHITE);
                        st.setMaxAlpha();
                        st.show();
                    } else {
                        if (email.getText().length() != 0 && !isValidEmail(email.getText().toString().trim())) {
                            email.setError("Invalid Email id");
                            StyleableToast st = new StyleableToast(RegisterActivity.this, "Inalid Email id !", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        } else if (state.getText().toString().equals("Select State")) {
                            StyleableToast st =
                                    new StyleableToast(RegisterActivity.this, "Select your state !", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        } else if (city.getText().toString().equals("")) {
                            city.setError("Enter your City");
                            StyleableToast st =
                                    new StyleableToast(RegisterActivity.this, "Enter your City !", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        } else if (pincode.getText().toString().equals("")) {
                            pincode.setError("Enter your Pincode");
                            StyleableToast st =
                                    new StyleableToast(RegisterActivity.this, "Enter your Pincode !", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        } else if (pannumber.getText().length() != 0 && !validatePanCard(pannumber.getText().toString().trim())) {
                            pannumber.setError("PAN number is not valid");
                            StyleableToast st = new StyleableToast(RegisterActivity.this,
                                    "PAN number is not valid !", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        } else if (usertype.getText().toString().equals("Wabco employee") && !isWabcoEmail(email.getText().toString().trim())) {
                            email.setError("Please enter WABCO email id");
                            StyleableToast st = new StyleableToast(RegisterActivity.this,
                                    "Please enter WABCO email id", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        } else if ((usertype.getText().toString().equals("Dealer") || usertype.getText().toString().equals("OEM Dealer"))
                                && (gstnumber.getText().toString().equals("") && dealercode.getText().toString().equals(""))) {
                            box.showAlertbox(getString(R.string.gst_validation_error));

                        } else {
                            if (gstnumber.getText().toString().length() != 0 && !validateGst(gstnumber.getText().toString())) {
                                gstnumber.setError("GST number is not valid");
                                StyleableToast st = new StyleableToast(RegisterActivity.this,
                                        "GST number is not valid !", Toast.LENGTH_SHORT);
                                st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                                st.setTextColor(Color.WHITE);
                                st.setMaxAlpha();
                                st.show();
                            } else if ((usertype.getText().toString().equals("Dealer") || usertype.getText().toString().equals("OEM Dealer"))) {

                                if ((Shop.getText().toString().equals(""))) {
                                    Shop.setError("Enter Your Shop name");
                                    StyleableToast st = new StyleableToast(RegisterActivity.this,
                                            "Please Enter Your Shop name!", Toast.LENGTH_SHORT);
                                    st.setBackgroundColor(RegisterActivity.this.getResources().getColor(R.color.red));
                                    st.setTextColor(Color.WHITE);
                                    st.setMaxAlpha();
                                    st.show();
                                } else
                                    CheckInternet();
                            } else {
                                CheckInternet();
                            }
                        }
                    }
                }
            }


        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                System.exit(0);
                finish();
            }
        });


        // FCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // gcm successfully registered
                // now subscribe to `global` topic to receive app wide notifications
                FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                displayFirebaseRegId();
            }
        };

        // displayFirebaseRegId();

    }

    private void getAscdetails() {
        final ProgressDialog loading =
                ProgressDialog.show(this, "ASCDetails", "Please wait...", false, false);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        token = pref.getString("regId", "notgen");
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        APIService api = retrofit.create(APIService.class);
        Call<ASCDetails> userapi = api.asccodecheck(dealercode.getText().toString());
        // Creating an anonymous callback
        userapi.enqueue(new Callback<ASCDetails>() {
            @Override
            public void onResponse(Call<ASCDetails> list, Response<ASCDetails> response) {
                if (response.body().getResult().equals("Valid User")) {
                    loading.dismiss();
                    name.setText(response.body().getData().getName());
                    phone.setText(response.body().getData().getMobileNumber());
                    email.setText(response.body().getData().getEmail());
                    city.setText(response.body().getData().getCity());
                    pincode.setText(response.body().getData().getZipcode());
                    Address.setText(response.body().getData().getAddress());
                    drawerResourcesList = getResources().getStringArray(R.array.state_array);
                    stateList = new ArrayList<String>();
                    stateList = Arrays.asList(drawerResourcesList);
                    drawerResourcesList[0] = response.body().getData().getState();
                    state.setItems(stateList);
                    state.setPadding(30, 0, 0, 0);

                } else if (response.body().getResult().equals("Invalid User")) {
                    loading.dismiss();
                    box.showAlertbox("ASC Code is Invalid. Please Check with WABCO Team");

                } else {
                    loading.dismiss();
                    box.showAlertbox("ASC Code is Invalid. Please Check with WABCO Team");

                }
            }

            @Override
            public void onFailure(Call<ASCDetails> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox("Failure Please Try Again Later");

            }
        });
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        token = pref.getString("regId", "notge");
        // Toast.makeText(FirstRegistration.this, token, Toast.LENGTH_SHORT).show();
        // box.showAlertbox(token);
        edit.putString("mobileid", token).commit();
        Log.e("FCM", "Firebase reg id: " + token);

    }

    public final static boolean isValidEmail(CharSequence target) {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isWabcoEmail(String target) {
        if (target.length() == 0) {
            return false;
        } else {
            String[] parts = target.split("@");
            String part2 = parts[1];
            if (part2.equals("wabco-auto.com")) {
                return true;
            } else {
                return false;

            }
        }
    }

    public boolean validateGst(final String gstnumber) {
        pattern = Pattern.compile(GST_PATTERN);
        matcher = pattern.matcher(gstnumber);
        boolean mat = matcher.matches();
        Log.v("GST Pattern", Boolean.toString(matcher.matches()));
        return matcher.matches();

    }


    public boolean validatePanCard(final String gstnumber) {

        pattern = Pattern.compile(PAN_PATTERN);
        matcher = pattern.matcher(gstnumber);
        return matcher.matches();

    }

    protected void CheckInternet() {
        // TODO Auto-generated method stub
        NetworkConnection isnet = new NetworkConnection(RegisterActivity.this);
        if (isnet.CheckInternet()) {
            // new AlreadyRegistered().execute();
            InsertUser();
        } else {
            showAlert(getResources().getString(R.string.nointernetmsg));
        }

    }

    public void InsertUser() {
        final ProgressDialog loading =
                ProgressDialog.show(this, "Registration", "Please wait...", false, false);

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        token = pref.getString("regId", "notgen");
        namestring = name.getText().toString();
        phonestring = phone.getText().toString();
        emailstring = email.getText().toString();
        citystring = city.getText().toString();
        pincodestring = pincode.getText().toString();
        dealercodestring = dealercode.getText().toString();
        countrystring = country.getText().toString();
        SelectedUser = usertype.getText().toString();
        ShopNameString = Shop.getText().toString();
        AddressString = Address.getText().toString();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        APIService api = retrofit.create(APIService.class);
        Call<User> userapi = api.InsetRegistration(namestring, emailstring, phonestring, SelectedUser,
                citystring, SelectedState, pincodestring, countrystring, currentDate, token, ShopNameString, AddressString,
                dealercode.getText().toString(), gstnumber.getText().toString(), pannumber.getText().toString(), "Android");
        // Creating an anonymous callback
        userapi.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> list, Response<User> response) {
                if (response.isSuccessful()) {
                    user = response.body();
                    loading.dismiss();
                    Log.v("Result", response.body().getResult());
                    if (response.body().getResult().equals("Registered")) {
                        SuccessRegistration(user);
                    } else if (response.body().getResult().equals("Updated")) {
                        SuccessRegistration(user);
//                        alreadyRegistration(user);
                       /* alertDialog = new Builder(RegisterActivity.this).create();
                        alertDialog.setTitle("WABCO");
                        alertDialog.setMessage("Already Registered! ");
                        alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();*/

                    } else if (response.body().getResult().equals("Invalid ASC")) {
                        box.showAlertbox("ASC Code is Invalid Please Check with Wabco Team");
                    } /*else {
                        loading.dismiss();
//                        Toast.makeText(getApplicationContext(),response.body().getResult(),Toast.LENGTH_LONG).show();
                        FailedRegistration();
                    }*/
                } else {
//                    Toast.makeText(getApplicationContext(),response.body().getResult(),Toast.LENGTH_LONG).show();
                    FailedRegistration("Failed to fetch data from the Server. Please try again later");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
//                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                FailedRegistration("No Internet Connection or Server is not responding.");
            }
        });
    }


    public void SuccessRegistration(final User user) {

        alertDialog = new Builder(RegisterActivity.this).create();
        alertDialog.setTitle("WABCO");
        alertDialog.setMessage("Registration Successful");
        alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                edit.putBoolean("isregistration", true);
                edit.putString("id", user.getData().getId());
                edit.putString("name", namestring);
                edit.putString("email", emailstring);
                edit.putString("phone", phonestring);
                edit.putString("usertype", SelectedUser);
                edit.putString("country", countrystring);
                edit.putString("state", SelectedState);
                edit.putInt("stateposition", SelectedStatePosition);
                edit.putInt("userposition", SelectedUserPosition);
                edit.putString("city", citystring);
                edit.putString("pincode", pincodestring);
                edit.putString("dealercode", dealercodestring);
                edit.putString("registereddate", currentDate);
                edit.putString("mobileid", token);
                edit.putString("shopname", ShopNameString);
                edit.putString("address", AddressString);
                edit.putString("gstnumber", gstnumber.getText().toString());
                edit.putString("pannumber", pannumber.getText().toString());
                edit.putString("username", "");
                edit.putString("password", "");
                edit.commit();

                Intent go = new Intent(RegisterActivity.this, HomePage.class);
                go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);

            }
        });
        alertDialog.show();

    }

    public void alreadyRegistration(final User user) {

        alertDialog = new Builder(RegisterActivity.this).create();
        alertDialog.setTitle("WABCO");
        alertDialog.setMessage("You Already Registred");
        alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                edit.putBoolean("isregistration", true);
                edit.putString("id", user.getData().getId());
                edit.putString("name", namestring);
                edit.putString("email", emailstring);
                edit.putString("phone", phonestring);
                edit.putString("usertype", SelectedUser);
                edit.putString("country", countrystring);
                edit.putString("state", SelectedState);
                edit.putInt("stateposition", SelectedStatePosition);
                edit.putInt("userposition", SelectedUserPosition);
                edit.putString("city", citystring);
                edit.putString("pincode", pincodestring);
                edit.putString("dealercode", dealercodestring);
                edit.putString("registereddate", currentDate);
                edit.putString("mobileid", token);
                edit.putString("shopname", ShopNameString);
                edit.putString("address", AddressString);
                edit.putString("gstnumber", gstnumber.getText().toString());
                edit.putString("pannumber", pannumber.getText().toString());
                edit.putString("username", "");
                edit.putString("password", "");
                edit.commit();

                /*Intent go = new Intent(RegisterActivity.this, HomePage.class);
                go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);*/

            }
        });
        alertDialog.show();

    }

    public void FailedRegistration(String msg) {
        alertDialog = new Builder(RegisterActivity.this).create();
        alertDialog.setTitle("WABCO");
        alertDialog.setMessage(msg);
        alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                alertDialog.dismiss();
                // edit.putBoolean("isregistration", true).commit();

               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission (
                            Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                        // SEE NEXT STEP
                        if (shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                            // show some description about this permission to the user as a dialog, toast or in
                            // Snackbar
                            alertDialog = new Builder(RegisterActivity.this).create();
                            alertDialog.setTitle("WABCO");
                            alertDialog.setMessage("You need to grant SEND SMS permission to send sms");
                            alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {

                                @RequiresApi(api = Build.VERSION_CODES.M)
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    requestPermissions(new String[]{Manifest.permission.SEND_SMS},
                                            PERMISSION_REQUEST);
                                }
                            });
                            alertDialog.show();

                        } else {
                            // request for the permission
                            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST);
                        }

                    } else {
                        sendSMS(wabcoNumber,
                                "Mr. " + name.getText().toString()
                                        + " is failed to Register WABCO Mobile Application without  internet.\n Mobile Number  : "
                                        + phone.getText().toString() + "\n Email Id  : " + email.getText().toString()
                                        + "\n City  : " + city.getText().toString() + "\n State  : " + SelectedState);
                    }

                } else {

                    sendSMS(wabcoNumber,
                            "Mr. " + name.getText().toString()
                                    + " is failed to Register WABCO Mobile Application without  internet.\n Mobile Number  : "
                                    + phone.getText().toString() + "\n Email Id  : " + email.getText().toString()
                                    + "\n City  : " + city.getText().toString() + "\n State  : " + SelectedState);

                }*/
            }
        });
        alertDialog.show();
    }

    public void sendSMS(final String phoneNo, final String msg) {
        try {


            namestring = name.getText().toString();
            phonestring = phone.getText().toString();
            emailstring = email.getText().toString();
            citystring = city.getText().toString();
            countrystring = city.getText().toString();

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);

            edit.putBoolean("isregistration", false);
            edit.putString("id", user.getData().getId());
            edit.putString("name", namestring);
            edit.putString("email", emailstring);
            edit.putString("phone", phonestring);
            edit.putString("usertype", SelectedUser);
            edit.putString("country", countrystring);
            edit.putString("state", SelectedState);
            edit.putInt("stateposition", SelectedStatePosition);
            edit.putInt("userposition", SelectedUserPosition);
            edit.putString("city", citystring);
            edit.putString("pincode", pincodestring);
            edit.putString("registereddate", currentDate);
            edit.putString("mobileid", token);
            edit.putString("shopname", ShopNameString);
            edit.putString("address", AddressString);
            edit.putString("gstnumber", gstnumber.getText().toString());
            edit.putString("pannumber", pannumber.getText().toString());
            edit.putString("username", "");
            edit.putString("password", "");
            edit.commit();


            alertDialog = new Builder(RegisterActivity.this).create();
            alertDialog.setTitle("WABCO");
            alertDialog.setMessage("Registration details has been sent to WABCO !\nThank you!");
            alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();

                    edit.putBoolean("isregistration", true).commit();

                    Log.v("Email phonumber name", msg);
                    Intent go = new Intent(RegisterActivity.this, HomePage.class);
                    go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(go);


                }
            });
            alertDialog.show();

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private void showAlert(String string) {
        // TODO Auto-generated method stub
        Alertbox alert = new Alertbox(RegisterActivity.this);
        alert.showAlertbox(string);
    }


   /* @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // reload my activity with permission granted or use the features what required the
                // permission

                sendSMS(wabcoNumber,
                        "Mr. " + name.getText().toString()
                                + " is failed to Register WABCO Mobile Application without  internet.\n Mobile Number  : "
                                + phone.getText().toString() + "\n Email Id  : " + email.getText().toString()
                                + "\n City  : " + city.getText().toString() + "\n State  : " + SelectedState);

            } else {
                Toast.makeText(RegisterActivity.this,
                        "The app was not allowed to send SMS to WABCO. Hence, it cannot function properly. Please consider granting it this permission",
                        Toast.LENGTH_LONG).show();
            }
        }


    }*/
}

