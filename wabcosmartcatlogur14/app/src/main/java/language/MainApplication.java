package language;

import android.app.Application;
import android.content.Context;

/**
 * Created by system01 on 7/1/2017.
 */

public class MainApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }
}
