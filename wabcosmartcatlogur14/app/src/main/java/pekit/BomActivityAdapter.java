package pekit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

public class BomActivityAdapter extends ArrayAdapter<String> {
    Context context;
    private List<String> descriptionList;
    private List<String> partNoList;
    private List<String> qtList;

    public BomActivityAdapter(Context context, List<String> partNoList, List<String> descriptionList, List<String> qtList) {
        super(context, R.layout.activity_bom_text, partNoList);
        this.context = context;
        this.partNoList = partNoList;
        this.descriptionList = descriptionList;
        this.qtList = qtList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        BomHolder bomHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.activity_bom_text, null);
            bomHolder = new BomHolder();
            bomHolder.sno = (TextView) convertView.findViewById(R.id.textView2);
            bomHolder.partno = (TextView) convertView.findViewById(R.id.textdeails);
            bomHolder.desc = (TextView) convertView.findViewById(R.id.textView3);
            bomHolder.qt = (TextView) convertView.findViewById(R.id.textView4);
            convertView.setTag(bomHolder);
        } else {
            bomHolder = (BomHolder) convertView.getTag();
        }
        bomHolder.sno.setText((position + 1) + ".");
        bomHolder.partno.setText(partNoList.get(position));
        bomHolder.desc.setText(descriptionList.get(position));
       bomHolder.qt.setText(qtList.get(position));
        return convertView;
    }
}
