package adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

import api.CustomItemClickListener;
import models.distributor.DistributorData;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by system01 on 6/12/2017.
 */

public class Select_Distributor_Adapter
    extends RecyclerView.Adapter<adapter.Select_Distributor_Adapter.Select_distributor_holder> {


  private List<DistributorData> data;
  private Context context;
  private int selectedPosition = -1;
  private CustomItemClickListener listener;
  private SharedPreferences distributor_share;
  private SharedPreferences.Editor distributor_edit;
  Select_distributor_holder quickholder;
  int pos;

  public Select_Distributor_Adapter(Context context, List<DistributorData> data) {
    this.context = context;
    this.data = data;
  }

  @Override
  public Select_Distributor_Adapter.Select_distributor_holder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    final View layoutView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.adapter_select_distributor_items, null);
    Select_Distributor_Adapter.Select_distributor_holder rcv =
        new Select_Distributor_Adapter.Select_distributor_holder(layoutView);
    final Select_distributor_holder mViewHolder = new Select_distributor_holder(layoutView);


    return rcv;
  }

  @Override
  public void onBindViewHolder(final Select_distributor_holder quickHolder, final int position) {

    distributor_share = context.getSharedPreferences("distributor_address", MODE_PRIVATE);
    distributor_edit = distributor_share.edit();
    quickholder = quickHolder;
    pos = position;
      quickHolder.Shop_name.setText(data.get(position).getName());
      quickHolder.Address.setText(data.get(position).getAddress());
      quickHolder.email.setText(data.get(position).getEmail());
    quickHolder.mobile.setText(data.get(position).getMobileNumber());

    if (data.get(position).getAddedfavorite()) {
      quickHolder.savefave_text.setText("Added to favorites");
      quickHolder.savefavorite.setVisibility(View.GONE);
      quickHolder.savefave_text.setVisibility(View.VISIBLE);
    } else {
      quickHolder.savefave_text.setText("Save to favorites");
      quickHolder.savefavorite.setVisibility(View.VISIBLE);
      quickHolder.savefave_text.setVisibility(View.GONE);
    }

    // check the radio button if both position and selectedPosition matches
    quickHolder.selectdistributor.setChecked(position == selectedPosition);
    // Set the position tag to both radio button and label
    quickHolder.selectdistributor.setTag(position);

    // in some cases, it will prevent unwanted situations
    quickHolder.savefavorite.setOnCheckedChangeListener(null);
    // if true, your checkbox will be selected, else unselected
    quickHolder.savefavorite.setChecked(data.get(position).getFavorite());
    quickHolder.savefavorite
        .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            data.get(position).setFavorite(isChecked);
            quickHolder.savefavorite.setChecked(isChecked);
          }
        });

    quickHolder.selectdistributor.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        itemCheckChanged(v);
      }
    });

    /*
     * if (quickholder.savefavorite.isChecked()) { quickholder.savefavorite.setChecked(false);
     * data.get(pos).setFavorite(false); } else { quickholder.savefavorite.setChecked(true);
     * data.get(pos).setFavorite(true); }
     */


  }


  // On selecting any view set the current position to selectedPositon and notify adapter
  public void itemCheckChanged(View v) {
    // data.get(selectedPosition).setSelected(true);

    selectedPosition = (Integer) v.getTag();

    notifyDataSetChanged();
    data.get(selectedPosition).setSelected(true);
    distributor_edit.clear();
    distributor_edit.commit();
    distributor_edit.putString("distributor_id", data.get(selectedPosition).getId().toString());
    distributor_edit.putString("distributor_name", data.get(selectedPosition).getName().toString());
    distributor_edit.putString("distributor_mobile", data.get(selectedPosition).getMobileNumber());
    distributor_edit.putString("distributor_email", data.get(selectedPosition).getEmail());
    distributor_edit.putString("distributor_address", data.get(selectedPosition).getAddress());
    distributor_edit.commit();

  }

  @Override
  public int getItemCount() {
    return (null != data ? data.size() : 0);
  }
  /*
   * public void setClickListener(CustomItemClickListener itemClickListener) { this.listener =
   * itemClickListener;
   * 
   * }
   */

  public class Select_distributor_holder extends RecyclerView.ViewHolder {
    public TextView Shop_name, Address, email, mobile, savefave_text;
    public CheckBox savefavorite, selectdistributor;


    public Select_distributor_holder(View v) {
      super(v);
      Shop_name = (TextView) v.findViewById(R.id.distributor_name);
      Address = (TextView) v.findViewById(R.id.distributor_address);
      email = (TextView) v.findViewById(R.id.distributor_email);
      mobile = (TextView) v.findViewById(R.id.distributor_mobile);
      savefave_text = (TextView) v.findViewById(R.id.savefave_text);
      savefavorite = (CheckBox) v.findViewById(R.id.savefave);
      selectdistributor = (CheckBox) v.findViewById(R.id.select_distributor);


    }


  }

}


