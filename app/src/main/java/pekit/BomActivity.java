package pekit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class BomActivity extends Activity {
    public List<String> PartNoList;
    private FloatingActionButton arrleft;
    private FloatingActionButton arrright;
    public List<String> descriptionList;

    private HorizontalScrollView hsv;
    public ProgressDialog loadDialog;
    private ListView partlist;
    private String partnunmber;
    public List<String> qtList;
    private ImageView vehicleBackImageView;
    private Alertbox box = new Alertbox(BomActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private ImageView Cart_Icon;
    private String UserType;
    private ListView pelist;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bom);
        hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        arrleft = (FloatingActionButton) findViewById(R.id.left);
        arrright = (FloatingActionButton) findViewById(R.id.right);
        vehicleBackImageView = (ImageView) findViewById(R.id.back);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
        pelist = (ListView) findViewById(R.id.listView);

        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);


        Cart_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if (cartDTO.getPartCodeList() == null) {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class)
                            .putExtra("from", "CartItem"));
                }
            }
        });



        vehicleBackImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });
        partnunmber = getIntent().getStringExtra("PartCode");
        TextView head = (TextView) findViewById(R.id.head_textView1);
        head.setText(partnunmber);
        Log.v("Part no frm intent", partnunmber);
        head.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();

            }
        });

        partlist = (ListView) findViewById(R.id.listView);
        new RetrievePartItemAsyn().execute(new String[]{partnunmber});


        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(BomActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(BomActivity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(BomActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(BomActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(BomActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(BomActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(BomActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(BomActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(BomActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(BomActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(BomActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }


    class RetrievePartItemAsyn extends AsyncTask<String, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(BomActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected String doInBackground(String... position) {
            PEkitDAO connection = new PEkitDAO(BomActivity.this);
            PEkitDTO pekitDTO = new PEkitDTO();
            pekitDTO = connection.retrievesservicepart(position[0]);
            PartNoList = pekitDTO.getPartNoList();
            descriptionList = pekitDTO.getdescriptionList();
            qtList = pekitDTO.getqtList();
            if (PartNoList.isEmpty()) {
                return "Empty";
            }
            Log.v("part no servicepart", (String) PartNoList.get(0));
            Log.v("RepaireKitNO ", (String) descriptionList.get(0));
            Log.v("Description servicepart", (String) qtList.get(0));
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                box.showAlertbox("Bom are Not Available !");
            }
            BomActivityAdapter service =
                    new BomActivityAdapter(BomActivity.this, PartNoList, descriptionList, qtList);
            arrright.setVisibility(View.VISIBLE);
            partlist.setAdapter(service);
        }
    }


    public void onBackPressed() {
        super.onBackPressed();
    }


}
