package pekit;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import persistence.DBHelper;


public class PEkitDAO {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public PEkitDAO(Context context) {
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    public void openDatabase() {
        this.db = this.dbHelper.readDataBase();
    }

    public void closeDatabase() {
        if (this.db != null) {
            this.db.close();
        }
    }

    public PEkitDTO retrieveALL() {
        List<String> PartnoList = new ArrayList<String>();
        List<String> Partcodeno = new ArrayList<String>();
        List<String> Descrip = new ArrayList<String>();
        List<String> flaglist = new ArrayList<String>();
        List<String> priceflaglist = new ArrayList<String>();
        PEkitDTO pekitDAO = new PEkitDTO();
        openDatabase();
       // db = SQLiteDatabase.openDatabase("/data/data/" + this.context.getPackageName() + "/app_wabcodb/wabco/", null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
        Cursor cursor = this.db.rawQuery("select distinct part_no,part_code_no,Description,used from pekit order by part_code_no asc", null);
        if (cursor.moveToFirst()) {
            do {
                PartnoList.add(cursor.getString(cursor.getColumnIndex("part_no")));
                Partcodeno.add(cursor.getString(cursor.getColumnIndex("part_code_no")));
                Descrip.add(cursor.getString(cursor.getColumnIndex("Description")));
                flaglist.add(cursor.getString(cursor.getColumnIndex("used")));
                priceflaglist.add(SetFlag(cursor.getString(cursor.getColumnIndex("part_no"))));
                Log.v("name", cursor.getString(cursor.getColumnIndex("part_no")));
                Log.v("id", cursor.getString(cursor.getColumnIndex("part_code_no")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        pekitDAO.setPartnoList(PartnoList);
        pekitDAO.setPartcodenoList(Partcodeno);
        pekitDAO.setDescripList(Descrip);
        pekitDAO.setFlagList(flaglist);
        pekitDAO.setPriceflaglist(priceflaglist);
        return pekitDAO;
    }



//Get Flag for pricelist
    public String SetFlag(String partno)
    {
        // TODO Auto-generated method stub
        openDatabase();

        String query ="select Part_No from pricelist where  Part_No ='" + partno + "' or Partcode  ='" + partno + "'";

        Cursor cursor = this.db.rawQuery(query, null);
        if (cursor.moveToFirst())
        {
            return "0";
        }
        else
        {
            return "1";
        }

    }

    public PEkitDTO retrievesservicepart(String Part_no) {
        List<String> bomPartNoList = new ArrayList<String>();
        List<String> bomDescriptionList = new ArrayList<String>();
        List<String> bomqtylist = new ArrayList<String>();
        List<String> flaglist = new ArrayList<String>();
        PEkitDTO pekitDTO = new PEkitDTO();
        openDatabase();
        Log.v("Query == ", "select distinct part_code_no,description,qty,used from bom where part_no='"+Part_no+"' order by part_code_no asc");
        
        Cursor cursor = this.db.rawQuery("select distinct part_code_no,description,qty from bom where part_no=? order by part_code_no asc", new String[]{Part_no});
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getPosition() >= 0) {
                    bomPartNoList.add(cursor.getString(cursor.getColumnIndex("part_code_no")));
                    bomDescriptionList.add(cursor.getString(cursor.getColumnIndex("description")));
                    bomqtylist.add(cursor.getString(cursor.getColumnIndex("qty")));
                    
                }
            } while (cursor.moveToNext());
        }
        closeDatabase();
        pekitDTO.setPartNoList(bomPartNoList);
        pekitDTO.setPartDespcriptionList(bomDescriptionList);
        pekitDTO.setProductservicepartNOList(bomqtylist);
        return pekitDTO;
    }
    
    
    public PEkitDTO retrievePurpose(String Part_no)
    {
        List<String> purposeDescp = new ArrayList<String>();
        
        PEkitDTO pekitDTO = new PEkitDTO();
        openDatabase();
        Log.v("Query == ", "select distinct description  from  purpose where part_no'"+Part_no+"' order by part_code_no asc");
        Cursor cursor = this.db.rawQuery("select distinct description  from  purpose where part_no = ?", new String[]{Part_no});
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getPosition() >= 0) {
                	purposeDescp.add(cursor.getString(cursor.getColumnIndex("description")));
                   
                }
            } while (cursor.moveToNext());
        }
        
        
        closeDatabase();
        pekitDTO.setPurposeDescpList(purposeDescp);
       
        return pekitDTO;
    }

	public PEkitDTO retrieveUSed(String Part_no) {
		// TODO Auto-generated method stub
		 List<String> usedDescp = new ArrayList<String>();
	        
	        PEkitDTO pekitDTO = new PEkitDTO();
	        openDatabase();
	        Log.v("Query == ", "select distinct assembly  from  used where part_no = '"+Part_no+"' order by part_code_no asc");
	        Cursor cursor = this.db.rawQuery("select distinct assembly  from  used where part_no = ?", new String[]{Part_no});
	        if (cursor.moveToFirst()) {
	            do {
	                if (cursor.getPosition() >= 0) {
	                	usedDescp.add(cursor.getString(cursor.getColumnIndex("assembly")));
	                   
	                }
	            } while (cursor.moveToNext());
	        }
	        
	        
	        closeDatabase();
	        pekitDTO.setusedAssList(usedDescp);
	       
	        return pekitDTO;
	}
    
}
