package addcart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.List;

import adapter.AddtoCart_Adapter;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import search.SearchDAO;
import vehiclemake.VehicleDAO;
import vehiclemake.VehicleDTO;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class    Add_TocartActivity extends Activity {

    private String partNumber, vehicleName, productName, vehicleId, productId;
    private ProgressDialog loading;
    private List<String> PartNoList, vehicleIDList, DescriptionList, ImageList, FlagList, productNameList, vehicleNameList, productIDList;
    private List<VehicleDTO> CartModel;
    private ImageView Backbtn;

    RecyclerView recycler_view;
    ImageView Cart_Icon;
    ImageView back;

    private VehicleDAO connection;
    private VehicleDTO vehicleDAO;
    private CartDAO cartDAO;
    private CartDTO cartDTO;
    private SearchDAO searchDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__tocart);

        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
        back = (ImageView) findViewById(R.id.back);
        recycler_view = (RecyclerView) findViewById(R.id.cart_view);

        PartNoList = new ArrayList<String>();
        vehicleIDList = new ArrayList<String>();
        DescriptionList = new ArrayList<String>();
        ImageList = new ArrayList<String>();
        FlagList = new ArrayList<String>();
        productNameList = new ArrayList<String>();
        vehicleNameList = new ArrayList<String>();
        productIDList = new ArrayList<String>();

        Intent partno = getIntent();

        if (partno.getStringExtra("from").equals("assembly")) {
           partNumber = partno.getStringExtra("PART_NO");
            vehicleName = partno.getStringExtra("vehicleName");
            productName = partno.getStringExtra("productName");
            vehicleId = partno.getStringExtra("vehicleIDList");
            productId = partno.getStringExtra("productIDList");
            new RetrievePartItemAsyn().execute();
        } else if (partno.getStringExtra("from").equals("vehicle")) {
            partNumber = partno.getStringExtra("PART_NO");

            new Retrievesevicerepair().execute();
        } else {
           partNumber = partno.getStringExtra("PART_NO");

            new RetrievePriceListItem().execute();
        }


        recycler_view.setHasFixedSize(true);
        recycler_view.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_view.setLayoutManager(llm);


        cartDAO = new CartDAO(Add_TocartActivity.this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if (cartDTO.getPartCodeList() == null) {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from", "CartItem"));
                }
                //startActivity(new Intent(Add_TocartActivity.this, Cart_Activity.class));
            }
        });



        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(Add_TocartActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(Add_TocartActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(Add_TocartActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(Add_TocartActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(Add_TocartActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(Add_TocartActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(Add_TocartActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(Add_TocartActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(Add_TocartActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(Add_TocartActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(Add_TocartActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });



    }

    private class Retrievesevicerepair extends AsyncTask<String, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(Add_TocartActivity.this, "Loading", "Please wait...", false, false);
            searchDAO = new SearchDAO(Add_TocartActivity.this);

        }

        protected String doInBackground(String... position) {
            try {
                cartDTO = searchDAO.retriveForPriceQuickOrder(partNumber);

                if (cartDTO == null) {
                    return "unsuccess";
                }
                return "success";

            } catch (Exception e) {
                Log.v("Error", e.getMessage());
                return "unsuccess";
            }
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loading.dismiss();

            AddtoCart_Adapter ca = new AddtoCart_Adapter(Add_TocartActivity.this, cartDTO, "pricelist");
            recycler_view.setAdapter(ca);
        }
    }

    private class RetrievePartItemAsyn extends AsyncTask<String, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(Add_TocartActivity.this, "Loading", "Please wait...", false, false);
            connection = new VehicleDAO(Add_TocartActivity.this);
            vehicleDAO = new VehicleDTO();
        }

        protected String doInBackground(String... position) {
            try {

                vehicleDAO = connection.retrievePartDetailsforCart(productId, vehicleId, productName, vehicleName);
                if (vehicleDAO == null) {
                    return "unsuccess";
                }
                return "success";

            } catch (Exception e) {
                Log.v("Error", e.getMessage());
                return "unsuccess";
            }
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loading.dismiss();

            AddtoCart_Adapter ca = new AddtoCart_Adapter(Add_TocartActivity.this, vehicleDAO, "assembly");
            recycler_view.setAdapter(ca);
        }
    }

    private class RetrievePriceListItem extends AsyncTask<String, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(Add_TocartActivity.this, "Loading", "Please wait...", false, false);
            searchDAO = new SearchDAO(Add_TocartActivity.this);
        }

        protected String doInBackground(String... position) {
            try {

                cartDTO = searchDAO.retriveForCartPriceList();
                if (cartDTO == null) {
                    return "unsuccess";
                }
                return "success";

            } catch (Exception e) {
                Log.v("Error", e.getMessage());
                return "unsuccess";
            }
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loading.dismiss();

            AddtoCart_Adapter ca = new AddtoCart_Adapter(Add_TocartActivity.this, cartDTO, "pricelist");
            recycler_view.setAdapter(ca);

        }
    }

}
