package pricelist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import addcart.Add_TocartActivity;
import wabco.ZoomActivity;

import static android.content.Context.MODE_PRIVATE;

public class PriceListAdapter extends ArrayAdapter<String> {
    private List<String> Partno;
    Context context;
    private List<String> partnumberList,partcodeList,  descriptionList,price_list,  mrpList,  catagoryList, imageList;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;

    public PriceListAdapter(Context context, List<String> partnumberList, List<String> partcodeList, List<String> descriptionList, List<String> price_list, List<String> mrpList, List<String> catagoryList, List<String> imageList) {
        super(context, R.layout.pricelist_item_text, partnumberList);
        this.partcodeList=partcodeList;
        this.partnumberList=partnumberList;
        this.descriptionList=descriptionList;
        this.price_list=price_list;
        this.mrpList = mrpList;
        this.catagoryList = catagoryList;
        this.context = context;
        this.imageList = imageList;
        myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        UserType = myshare.getString("usertype","").toString();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        PriceHolderservice viewHolderservice;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.pricelist_item_text, null);
            viewHolderservice = new PriceHolderservice();
            viewHolderservice.sno = (TextView) convertView.findViewById(R.id.textsno);
            viewHolderservice.partcodetxt = (TextView) convertView.findViewById(R.id.partcode);
            viewHolderservice.partnumbertxt = (TextView) convertView.findViewById(R.id.partno);
            viewHolderservice.descriptiontxt = (TextView) convertView.findViewById(R.id.description);
            viewHolderservice.pricetxt = (TextView) convertView.findViewById(R.id.price);
            viewHolderservice.mrptxt = (TextView) convertView.findViewById(R.id.mrp);
            viewHolderservice.catagorytxt = (TextView) convertView.findViewById(R.id.catagory);
            viewHolderservice.imageView = (ImageView) convertView.findViewById(R.id.image);
//            viewHolderservice.addcart = (Button) convertView.findViewById(R.id.addcart);

            convertView.setTag(viewHolderservice);
        } else {
            viewHolderservice = (PriceHolderservice) convertView.getTag();
        }
//        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
//        {
//            viewHolderservice.addcart.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            viewHolderservice.addcart.setVisibility(View.GONE);
//        }
        viewHolderservice.sno.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());
        viewHolderservice.partcodetxt.setText(partcodeList.get(position));
        viewHolderservice.partnumbertxt.setText(partnumberList.get(position));
        viewHolderservice.descriptiontxt.setText(descriptionList.get(position));
        viewHolderservice.pricetxt.setText(context.getResources().getString(R.string.Rs)+" "+price_list.get(position));
        viewHolderservice.mrptxt.setText(context.getResources().getString(R.string.Rs)+" "+mrpList.get(position));
        viewHolderservice.catagorytxt.setText(catagoryList.get(position));

        try {
            if(Arrays.asList(context.getResources().getAssets().list("")).contains(imageList.get(position)+".jpg")) {

                Picasso.with(context).load("file:///android_asset/" + imageList.get(position)+".jpg").error(R.drawable.noimagefound).into(viewHolderservice.imageView);
            }
            else {
                Picasso.with(context).load("file:///android_asset/" + "noimagefound.jpg").error(R.drawable.noimagefound).into(viewHolderservice.imageView);
            }
        } catch (IOException e) {

            e.printStackTrace();
        }




      /*  if (imageList.get(position).equals("")) {
            Picasso.with(context).load("file:///android_asset/noimagefound.jpg").into(viewHolderservice.imageView);
        } else {
            Picasso.with(context).load("file:///android_asset/" + imageList.get(position) + ".jpg").into(viewHolderservice.imageView);
        }
*/

        viewHolderservice.imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //expandedImageView.setImageBitmap(logoimage.get(position));
                if(imageList.get(position).equals(""))
                {
                    //showAlert(partdespTextList.get(position)+" Image not found ");
                    Toast.makeText(context, descriptionList.get(position)+" Image not found ", Toast.LENGTH_LONG).show();
                }
                else {
                    Intent goZoom = new Intent(context, ZoomActivity.class);
                    goZoom.putExtra("ImageName", imageList.get(position));
                    goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(goZoom);
                }

            }
        });
//        viewHolderservice.addcart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent gotocart = new Intent(context, Add_TocartActivity.class);
//               /* if (SearchFrom.equals("service")){
//                gotocart.putExtra("PART_NO", service_partno.get(position));}
//                else{*/
//                gotocart.putExtra("PART_NO", partnumberList.get(position));
//                gotocart.putExtra("from", "vehicle");
//                context.startActivity(gotocart);
//            }
//        });


        return convertView;
    }




    class PriceHolderservice {
        TextView partcodetxt,partnumbertxt,descriptiontxt,pricetxt,mrptxt,catagorytxt,sno;
        ImageView imageView;
//        Button addcart;

    }

}
