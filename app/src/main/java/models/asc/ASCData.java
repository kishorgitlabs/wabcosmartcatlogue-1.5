
package models.asc;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ASCData {

    @SerializedName("ASC_Code")
    private String mASCCode;
    @SerializedName("Address")
    private String mAddress;
    @SerializedName("centralizedid")
    private String mCentralizedid;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("Division")
    private String mDivision;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("FEnigineerName")
    private String mFEnigineerName;
    @SerializedName("FEnigineerTown")
    private String mFEnigineerTown;
    @SerializedName("FEnigineerZone")
    private String mFEnigineerZone;
    @SerializedName("GSTID")
    private String mGSTID;
    @SerializedName("id")
    private Long mId;
    @SerializedName("MobileNumber")
    private String mMobileNumber;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("Zipcode")
    private String mZipcode;

    public String getASCCode() {
        return mASCCode;
    }

    public void setASCCode(String aSCCode) {
        mASCCode = aSCCode;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCentralizedid() {
        return mCentralizedid;
    }

    public void setCentralizedid(String centralizedid) {
        mCentralizedid = centralizedid;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getDivision() {
        return mDivision;
    }

    public void setDivision(String division) {
        mDivision = division;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFEnigineerName() {
        return mFEnigineerName;
    }

    public void setFEnigineerName(String fEnigineerName) {
        mFEnigineerName = fEnigineerName;
    }

    public String getFEnigineerTown() {
        return mFEnigineerTown;
    }

    public void setFEnigineerTown(String fEnigineerTown) {
        mFEnigineerTown = fEnigineerTown;
    }

    public String getFEnigineerZone() {
        return mFEnigineerZone;
    }

    public void setFEnigineerZone(String fEnigineerZone) {
        mFEnigineerZone = fEnigineerZone;
    }

    public String getGSTID() {
        return mGSTID;
    }

    public void setGSTID(String gSTID) {
        mGSTID = gSTID;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMobileNumber() {
        return mMobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        mMobileNumber = mobileNumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        mUpdateDate = updateDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getZipcode() {
        return mZipcode;
    }

    public void setZipcode(String zipcode) {
        mZipcode = zipcode;
    }

}
