
package models.usermodel;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("data")
    private Data mData;
    @SerializedName("result")
    private String mResult;

//    public User(User other) {
//        this.mData = other.mData;
//        this.mResult = other.mResult;
//    }



    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
