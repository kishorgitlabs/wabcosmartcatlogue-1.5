package models.quickorder.request;

/**
 * Created by system01 on 6/19/2017.
 */

public class QuickOrder {


    private Order Order;
    private String result;
    private Order_Parts[] Order_Parts;


    public String getResult ()
    {
        return result;
    }

    public void setResult (String result)
    {
        this.result = result;
    }

    public Order getOrder ()
    {
        return Order;
    }

    public void setOrder (Order Order)
    {
        this.Order = Order;
    }

    public Order_Parts[] getOrder_Parts ()
    {
        return Order_Parts;
    }

    public void setOrder_Parts (Order_Parts[] Order_Parts)
    {
        this.Order_Parts = Order_Parts;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Order = "+Order+", Order_Parts = "+Order_Parts+"]";
    }
}
