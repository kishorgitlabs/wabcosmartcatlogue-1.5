
package models.genunecheck;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class WabcoEmployee {

    @SerializedName("data")
    private List<Wabcoemployeelist> mData;
    @SerializedName("result")
    private String mResult;

    public List<Wabcoemployeelist> getData() {
        return mData;
    }

    public void setData(List<Wabcoemployeelist> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
