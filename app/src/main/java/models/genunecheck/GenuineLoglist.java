
package models.genunecheck;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class GenuineLoglist implements Serializable {

    @SerializedName("City")
    private String mCity;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("ID")
    private Long mID;
    @SerializedName("IsScanned")
    private Boolean mIsScanned;
    @SerializedName("pickid")
    private String mPickid;
    @SerializedName("ScannedBy")
    private String mScannedBy;
    @SerializedName("SerialID")
    private String mSerialID;
    @SerializedName("State")
    private String mState;
    @SerializedName("Type")
    private String mType;
    @SerializedName("UniqueID")
    private String mUniqueID;
    @SerializedName("UserType")
    private String mUserType;
    @SerializedName("userid")
    private String mUserid;
    @SerializedName("TransactionAddress")
    private String mTransactionAddress;

    @SerializedName("CustomerName")
    private String mCustomerName;

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public String getTransactionAddress() {
        return mTransactionAddress;
    }

    public void setTransactionAddress(String TransactionAddress) {
        mTransactionAddress = TransactionAddress;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public Long getID() {
        return mID;
    }

    public void setID(Long ID) {
        mID = ID;
    }

    public Boolean getIsScanned() {
        return mIsScanned;
    }

    public void setIsScanned(Boolean IsScanned) {
        mIsScanned = IsScanned;
    }

    public String getPickid() {
        return mPickid;
    }

    public void setPickid(String pickid) {
        mPickid = pickid;
    }

    public String getScannedBy() {
        return mScannedBy;
    }

    public void setScannedBy(String ScannedBy) {
        mScannedBy = ScannedBy;
    }

    public String getSerialID() {
        return mSerialID;
    }

    public void setSerialID(String SerialID) {
        mSerialID = SerialID;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getType() {
        return mType;
    }

    public void setType(String Type) {
        mType = Type;
    }

    public String getUniqueID() {
        return mUniqueID;
    }

    public void setUniqueID(String UniqueID) {
        mUniqueID = UniqueID;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String UserType) {
        mUserType = UserType;
    }

    public String getUserid() {
        return mUserid;
    }

    public void setUserid(String userid) {
        mUserid = userid;
    }

}
