
package models.genunecheck;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class UpdateUser {

    @SerializedName("data")
    private UserData mData;
    @SerializedName("result")
    private String mResult;

    public UserData getData() {
        return mData;
    }

    public void setData(UserData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
