
package models.dealer.pending.request.cancel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Cancel {

    @SerializedName("Dealerid")
    private String mDealerid;
    @SerializedName("Distributorid")
    private String mDistributorid;
    @SerializedName("OrderNumber")
    private String mOrderNumber;
    @SerializedName("OrderStatus")
    private String mOrderStatus;
    @SerializedName("UpdatedDate")
    private String mUpdatedDate;

    @SerializedName("Active")
    private String Active;


    public String getDealerid() {
        return mDealerid;
    }

    public void setDealerid(String Dealerid) {
        mDealerid = Dealerid;
    }

    public String getDistributorid() {
        return mDistributorid;
    }

    public void setDistributorid(String Distributorid) {
        mDistributorid = Distributorid;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String OrderStatus) {
        mOrderStatus = OrderStatus;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }
    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }
}
