
package models.dealer.pending.request.cancel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class CancelOrder {

    @SerializedName("Cancel")
    private Cancel mCancel;
    @SerializedName("Cancel_Parts")
    private List<CancelPart> mCancelParts;

    public Cancel getCancel() {
        return mCancel;
    }

    public void setCancel(Cancel Cancel) {
        mCancel = Cancel;
    }

    public List<CancelPart> getCancelParts() {
        return mCancelParts;
    }

    public void setCancelParts(List<CancelPart> CancelParts) {
        mCancelParts = CancelParts;
    }

}
