
package models.distributor;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class DistributorData {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("CreatedDate")
    private String mCreatedDate;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("FEnigineerName")
    private String mFEnigineerName;
    @SerializedName("FEnigineerTown")
    private String mFEnigineerTown;
    @SerializedName("FEnigineerZone")
    private String mFEnigineerZone;
    @SerializedName("GSTID")
    private String mGSTID;
    @SerializedName("id")
    private String mId;
    @SerializedName("MobileNumber")
    private String mMobileNumber;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private String mUpdateDate;
    @SerializedName("UserName")
    private String mUserName;
    @SerializedName("Zipcode")
    private String mZipcode;
    private Boolean favorite=false;


    private Boolean addedfavorite=false;

    private Boolean isSelected=false;


    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }



    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }



    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String Country) {
        mCountry = Country;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        mCreatedDate = CreatedDate;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getFEnigineerName() {
        return mFEnigineerName;
    }

    public void setFEnigineerName(String FEnigineerName) {
        mFEnigineerName = FEnigineerName;
    }

    public String getFEnigineerTown() {
        return mFEnigineerTown;
    }

    public void setFEnigineerTown(String FEnigineerTown) {
        mFEnigineerTown = FEnigineerTown;
    }

    public String getFEnigineerZone() {
        return mFEnigineerZone;
    }

    public void setFEnigineerZone(String FEnigineerZone) {
        mFEnigineerZone = FEnigineerZone;
    }

    public String getGSTID() {
        return mGSTID;
    }

    public void setGSTID(String GSTID) {
        mGSTID = GSTID;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getMobileNumber() {
        return mMobileNumber;
    }

    public void setMobileNumber(String MobileNumber) {
        mMobileNumber = MobileNumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        mName = Name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String Password) {
        mPassword = Password;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String Region) {
        mRegion = Region;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String UpdateDate) {
        mUpdateDate = UpdateDate;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String UserName) {
        mUserName = UserName;
    }

    public String getZipcode() {
        return mZipcode;
    }

    public void setZipcode(String Zipcode) {
        mZipcode = Zipcode;
    }

    public Boolean getAddedfavorite() {
        return addedfavorite;
    }

    public void setAddedfavorite(Boolean addedfavorite) {
        this.addedfavorite = addedfavorite;
    }

}
