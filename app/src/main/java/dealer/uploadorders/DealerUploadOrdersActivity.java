package dealer.uploadorders;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;

import adapter.Offline_Orderlist_Adapter;
import askwabco.AskWabcoActivity;
import dealer.account.Dealer_Account_Activity;
import directory.WabcoUpdate;
import home.MainActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class DealerUploadOrdersActivity extends AppCompatActivity {

    private View heade_Layout;
    private ImageView Backbtn, Accountbtn,Cart_Icon,Menu;
    private TextView Tittle,Username;

    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private ListView listview;
    private ArrayList<String> Dis_name,Dis_id,Dis_addres;
    Offline_Orderlist_Adapter Adapter;
    NetworkConnection isnet = new NetworkConnection(DealerUploadOrdersActivity.this);
    private ImageView Menubtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer_upload_orders);


        //Default_wishlist_layout = (LinearLayout) findViewById(R.id.default_wishlist_layout);
        listview = (ListView) findViewById(R.id.order_list);
        Dis_name = new ArrayList<String>();
        Dis_id =  new ArrayList<String>();
        Dis_addres =  new ArrayList<String>();

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        Accountbtn = (ImageView) heade_Layout.findViewById(R.id.account_icon);
        Cart_Icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);
        Menu = (ImageView) heade_Layout.findViewById(R.id.menu);

        dbHelper = new DBHelper(getApplicationContext());
        Tittle.setText("Offline Orders");
        Cart_Icon.setVisibility(View.INVISIBLE);
        Accountbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(
                        new Intent(getApplicationContext(), Dealer_Account_Activity.class));
            }
        });

        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });




        Menubtn = (ImageView) heade_Layout.findViewById(R.id.menu);
        Menubtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(DealerUploadOrdersActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(DealerUploadOrdersActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(DealerUploadOrdersActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });

      //  Delete_QuickOrder();
        new GetOrderlist().execute();
    }



    private class GetOrderlist extends AsyncTask<String, Void, String> {

        Cursor cursor;
        ProgressDialog progressDialog;

        protected void onPreExecute() {
            super.onPreExecute();
              /*  progressDialog =
                    ProgressDialog.show(DealerUploadOrdersActivity.this, "" +
                            "Loading", "Please wait...", false, false);*/
            db = dbHelper.readDataBase();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                String query = "select distinct dis_name,distributor_id,dis_address from Offlineorders";
                Log.v("Offlineorders", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        Dis_name.add(cursor.getString(cursor.getColumnIndex("dis_name")));
                        Dis_id.add(cursor.getString(cursor.getColumnIndex("distributor_id")));
                        Dis_addres.add(cursor.getString(cursor.getColumnIndex("dis_address")));
                    } while (cursor.moveToNext());
                    cursor.close();
                    db.close();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                cursor.close();
                db.close();
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           // progressDialog.dismiss();

            switch (result) {
                case "received":
                    Adapter = new Offline_Orderlist_Adapter(DealerUploadOrdersActivity.this, Dis_name,Dis_id,Dis_addres);
                    listview.setAdapter(Adapter);
                    break;
                case "nodata":
                    final AlertDialog.Builder alertDialog =
                            new AlertDialog.Builder(DealerUploadOrdersActivity.this);
                    alertDialog.setTitle("WABCO");
                    alertDialog.setMessage("No offline orders found !");
                    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });

                    alertDialog.show();
                    break;
                default:

                    break;
            }

        }

    }
    private void Delete_QuickOrder()
    {
        try {
            dbHelper = new DBHelper(getApplicationContext());
            db = dbHelper.readDataBase();
            db.execSQL("DROP TABLE Offlineorders");
            db.close();
            Toast.makeText(getApplicationContext(),"Successful",Toast.LENGTH_LONG).show();
        }catch (Exception ex)
        {
            Log.v("Delete", ex.getMessage());
        }
    }

}
/* Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
                //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });

          private void Delete_QuickOrder()
    {
        try {
        dbHelper = new DBHelper(getApplicationContext());
        db = dbHelper.readDataBase();
        db.execSQL("DROP TABLE Offlineorders");
        db.close();
        Toast.makeText(getApplicationContext(),"Successful",Toast.LENGTH_LONG).show();
    }catch (Exception ex)
    {
        Log.v("Delete", ex.getMessage());
    }
    }


*/