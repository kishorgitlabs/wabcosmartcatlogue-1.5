package contact;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.wabco.brainmagic.wabco.catalogue.R;

public class ContactAdapter extends ArrayAdapter<String> {
    private List<String> address;
    private List<String> cityList;
    private Context context;
    private List<String> distributor;
    private List<String> email1;
    private List<String> email2;
    private List<String> email3;
    private List<String> phone;
    private List<String> stateName;
    private List<String> contact_type;

    public ContactAdapter(Context context, List<String> stateName, List<String> cityList, List<String> distributor, List<String> address, List<String> phone, List<String> email1, List<String> email2, List<String> email3,List<String> contact_type) {
        super(context, R.layout.activity_contact_text, stateName);
        this.context = context;
        this.stateName = stateName;
        this.cityList = cityList;
        this.distributor = distributor;
        this.address = address;
        this.phone = phone;
        this.email1 = email1;
        this.email2 = email2;
        this.email3 = email3;
        this.contact_type=contact_type;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ContactHolder contactHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.activity_contact_text, null);
            contactHolder = new ContactHolder();
            contactHolder.so = (TextView) convertView.findViewById(R.id.textView2);
            contactHolder.City = (TextView) convertView.findViewById(R.id.city);
            contactHolder.Ditribtor = (TextView) convertView.findViewById(R.id.textView3);
            contactHolder.Address = (TextView) convertView.findViewById(R.id.textView4);
            contactHolder.Phone = (TextView) convertView.findViewById(R.id.textView6);
            contactHolder.Eail1 = (TextView) convertView.findViewById(R.id.email1);
            contactHolder.Eail2 = (TextView) convertView.findViewById(R.id.email2);
            contactHolder.Eail3 = (TextView) convertView.findViewById(R.id.email3);
            contactHolder.location = (ImageView) convertView.findViewById(R.id.map);
            convertView.setTag(contactHolder);
        }
        else
        {
            contactHolder = (ContactHolder) convertView.getTag();
        }
        contactHolder.so.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());
        contactHolder.Ditribtor.setText((CharSequence) this.distributor.get(position));
        contactHolder.Address.setText((CharSequence) this.address.get(position));
        contactHolder.Phone.setText((CharSequence) this.phone.get(position));
        contactHolder.Eail1.setText((CharSequence) this.email1.get(position));
        contactHolder.Eail2.setText((CharSequence) this.email2.get(position));
        contactHolder.Eail3.setText((CharSequence) this.email3.get(position));
        contactHolder.City.setText((CharSequence) this.cityList.get(position));

        contactHolder.location.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + address.get(position).toString()));
			        context.startActivity(searchAddress);
			}
		});

        return convertView;

    }

}
