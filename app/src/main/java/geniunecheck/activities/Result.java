package geniunecheck.activities;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

public class Result extends AppCompatActivity {
    ImageView res_iv;
    TextView tv_sat, tv_sat_hin, tv_msg_hin;
    TextView tv_msg, tv_report, tv_gen;
    String Result, count;
    String DLcount, FLOcount, WScount, GMcount,Serialid,Uniqid,picid;
    TextView tv_slid, tv_unid,note;
    LinearLayout ly_unq,ly_sl;
    RelativeLayout r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        res_iv = (ImageView) findViewById(R.id.image_vw);
        tv_slid = (TextView) findViewById(R.id.edt_serlno);
        tv_unid = (TextView) findViewById(R.id.edt_uniqid);
        tv_sat = (TextView) findViewById(R.id.status);
        tv_msg = (TextView) findViewById(R.id.message);
        tv_gen = (TextView) findViewById(R.id.gen);
        ly_sl = (LinearLayout) findViewById(R.id.ly_sl);
        ly_unq = (LinearLayout) findViewById(R.id.ly_unq);
        r = (RelativeLayout) findViewById(R.id.note);
        note = (TextView) findViewById(R.id.notetv);
        r.setVisibility(View.GONE);
     /*   tv_sat_hin = (TextView) findViewById(R.id.statushindi);
        tv_msg_hin = (TextView) findViewById(R.id.messagehindi);*/

        tv_report = (TextView) findViewById(R.id.report);
        Intent i = getIntent();
        Result = i.getStringExtra("Result");
        count = i.getStringExtra("count");
        DLcount = i.getStringExtra("DLcount");
        GMcount = i.getStringExtra("GMcount");
        FLOcount = i.getStringExtra("FLOcount");
        WScount = i.getStringExtra("WScount");
        Serialid = i.getStringExtra("Serialid");
        Uniqid = i.getStringExtra("Uniqueid");
        picid = i.getStringExtra("picid");
        tv_slid.setText(Serialid);
        tv_unid.setText(Uniqid);
        if (Result.equals("Unique ID  not Found")) {
            res_iv.setImageResource(R.drawable.closegood);
            tv_sat.setText("Unique ID  not found for this Serial Number " + Serialid);
            tv_sat.setTextColor(Color.parseColor("#dc002e"));
            tv_msg.setVisibility(View.GONE);
            ly_sl.setVisibility(View.GONE);
            ly_unq.setVisibility(View.GONE);

        } else if (Result.equals("Geniune")) {
            res_iv.setImageResource(R.drawable.tickgr);
            tv_sat.setText("Geniune. ");
            tv_sat.setTextColor(Color.parseColor("#2c924c"));
            tv_gen.setText("Happy buying WABCO product.");
            tv_report.setVisibility(View.GONE);
            tv_msg.setVisibility(View.GONE);

        } else if (Result.equals("Suspecious")) {
            res_iv.setImageResource(R.drawable.warn);
            tv_sat.setText("Suspicious.");
            tv_gen.setVisibility(View.GONE);
            tv_sat.setTextColor(Color.parseColor("#ffb733"));
            tv_msg.setText("Check with WABCO customer care." +
                    "\nThis product already scanned by " + '\b' + WScount + '\b' + " Whole Sale Distributor" +
                    "\nThis product already scanned by " + '\b' + DLcount + '\b' + " Dealer" +
                    "\nThis product already scanned by " + '\b' + GMcount + '\b' + " Garage Mechanic" +
                    "\nThis product already scanned by " + '\b' + FLOcount + '\b' + " Fleet Operator");
//            String note="<font color= #4b4b4b>Check with WABCO customer care. </font>\n" +
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+WScount+"</font><font color= #4b4b4b> Whole Sale Distributor</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+DLcount+"</font><font color= #4b4b4b> Dealer</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+GMcount+"</font><font color= #4b4b4b> Garage Mechanic</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+FLOcount+"</font><font color= #4b4b4b> Fleet Operator</font>\n";
//            tv_msg.setText(Html.fromHtml(note));
        } else if (Result.equals("Highly Suscpcious")) {
            res_iv.setImageResource(R.drawable.closegood);
            tv_sat.setText("Highly Suspicious.");
            tv_sat.setTextColor(Color.parseColor("#dc002e"));
            tv_gen.setVisibility(View.GONE);
            tv_msg.setText("Not recommended." +
                    "\nThis product already scanned by " + '\b' + WScount + '\b' + " Whole Sale Distributor" +
                    "\nThis product already scanned by " + '\b' + DLcount + '\b' + " Dealer" +
                    "\nThis product already scanned by " + '\b' + GMcount + '\b' + " Garage Mechanic" +
                    "\nThis product already scanned by " + '\b' + FLOcount + '\b' + " Fleet Operator");
//            String note="<font color= #4b4b4b>Check with WABCO customer care.</font>\n" +
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+WScount+"</font><font color= #4b4b4b> Whole Sale Distributor</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+DLcount+"</font><font color= #4b4b4b> Dealer</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+GMcount+"</font><font color= #4b4b4b> Garage Mechanic</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by </font><font color= #dc002e>"+FLOcount+"</font><font color= #4b4b4b> Fleet Operator</font>\n";
//            tv_msg.setText(Html.fromHtml(note));

        } else if (Result.equals("Already Scanned by You")) {
            res_iv.setImageResource(R.drawable.warn);
            tv_sat.setText("Already Scanned by You.");
            tv_msg.setText("");
            tv_gen.setVisibility(View.GONE);
            tv_sat.setTextColor(Color.parseColor("#ffb733"));
//            tv_msg.setText("Check with WABCO customer care." +
//                    "\nThis product already scanned by "+'\b'+WScount+'\b'+" Whole Sale Distributor" +
//                    "\nThis product already scanned by " + '\b'+DLcount +'\b'+ " Dealer" +
//                    "\nThis product already scanned by " + '\b'+GMcount + '\b'+" Garage Mechanic" +
//                    "\nThis product already scanned by " + '\b'+FLOcount + '\b'+" Fleet Operator");

//            String note="<font color= #4b4b4b>Check with WABCO customer care.</font>\n" +
//                    "<font color= #4b4b4b>This product already scanned by</font><font color= #dc002e>"+WScount+"</font><font color= #4b4b4b>Whole Sale Distributor</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by</font><font color= #dc002e>"+DLcount+"</font><font color= #4b4b4b>Dealer</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by</font><font color= #dc002e>"+GMcount+"</font><font color= #4b4b4b> Garage Mechanic</font>\n"+
//                    "<font color= #4b4b4b>This product already scanned by</font><font color= #dc002e>"+FLOcount+"</font><font color= #4b4b4b> Fleet Operator</font>\n";
//            tv_msg.setText(Html.fromHtml(note));


        } else if (Result.equals("No")) {
            res_iv.setImageResource(R.drawable.closegood);
            tv_sat.setText("Duplicate Part\nPlease Do not buy");
            tv_sat.setTextColor(Color.parseColor("#dc002e"));
            tv_msg.setText("डुप्लिकेट भाग। सुझाए गए नहीं खरीदें");
            tv_msg.setTextColor(Color.parseColor("#dc002e"));
            ly_sl.setVisibility(View.GONE);
            ly_unq.setVisibility(View.GONE);

        }
        if ((Result.equals("Geniune"))) {
            if ((picid.isEmpty())) {
                r.setVisibility(View.VISIBLE);
            } else {
                r.setVisibility(View.GONE);
            }
        }
    }
    @Override
    public void onBackPressed() {
        Intent i=new Intent(Result.this ,OriginalParts.class);
        startActivity(i);

    }
}
