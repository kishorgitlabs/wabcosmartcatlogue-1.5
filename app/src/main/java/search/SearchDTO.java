package search;

import java.util.ArrayList;
import java.util.List;

public class SearchDTO {
    private List<String> Flag_List;
    private List<String> descrip;
    private List<String> description_List;
    private List<String> image_List;
    private List<String> partcodeno;
    private List<String> partnoList;
    private String productNameList;
    private List<String> productPartno_List;
    private List<String> vehicleNameList;
    private List<String> vehiclePartNoList;
    private List<String> productNameList2;
    private List<String> productIDList;
    private List<String> vehicleIDList;
    private List<String> partCodeList;
    private List<String> price_List;
    private List<String> mrpList;
    private List<String> catagoryList;
    private List<String> imageList;
    private List<String> despcriptionList;
    private ArrayList<String> autocompletePartNoList,autocompleteDespcriptionList,autocompletepartcode;
    private List<String> priceflaglist;

    public ArrayList<String> getAutocompletepartcode() {
        return autocompletepartcode;
    }

    public void setAutocompletepartcode(ArrayList<String> autocompletepartcode) {
        this.autocompletepartcode = autocompletepartcode;
    }

    private List<String> originalList;
    private List<String> equivalentList;


    public List<String> getPriceflaglist() {
        return priceflaglist;
    }

    public void setPriceflaglist(List<String> priceflaglist) {
        this.priceflaglist = priceflaglist;
    }

    public List<String> getPartno_List() {
        return this.productPartno_List;
    }

    public void setPartno_List(List<String> partIDList) {
        this.productPartno_List = partIDList;
    }

    public void setProductNameList(String productNameList2) {
        this.productNameList = productNameList2;
    }

    public void setVehicleNameList(List<String> vehicleNameList) {
        this.vehicleNameList = vehicleNameList;
    }

    public void setPart_No_List(List<String> productPartno_List) {
        this.productPartno_List = productPartno_List;
    }

    public void setDescription(List<String> description_List) {
        this.description_List = description_List;
    }

    public void setService_Part(List<String> image_List) {
        this.image_List = image_List;
    }

    public List<String> getVehicleName() {
        return this.vehicleNameList;
    }

    public List<String> getDescription() {
        return this.description_List;
    }

    public List<String> getService_List() {
        return this.image_List;
    }

    public String getProductNameList() {
        return this.productNameList;
    }

    public void setPart_A(List<String> vehiclePartNoList) {
        this.vehiclePartNoList = vehiclePartNoList;
    }

    public List<String> getPartno_A() {
        return this.vehiclePartNoList;
    }

    public void setFlagList(List<String> flag_List) {
        this.Flag_List = flag_List;
    }

    public List<String> getFlag() {
        return this.Flag_List;
    }

    public void setPartNoList(List<String> partnoList) {
        this.partnoList = partnoList;
    }

    public void setPartDespcriptionList(List<String> partcodeno) {
        this.partcodeno = partcodeno;
    }

    public void setProductservicepartNOList(List<String> descrip) {
        this.descrip = descrip;
    }

    public List<String> getpartnoList() {
        return this.partnoList;
    }

    public List<String> getpartcodeno() {
        return this.partcodeno;
    }

    public List<String> getdescrip() {
        return this.descrip;
    }

    public void setProductList(List<String> productNameList2) {
        // TODO Auto-generated method stub
        this.productNameList2 = productNameList2;
    }

    public void setProductIdList(List<String> productIDList) {
        this.productIDList = productIDList;
        // TODO Auto-generated method stub

    }

    public void setVehicleIDList(List<String> vehicleIDList) {
        // TODO Auto-generated method stub
        this.vehicleIDList = vehicleIDList;
    }

    public List<String> getProductIDList() {
        // TODO Auto-generated method stub
        return productIDList;
    }

    public List<String> getProductNameList2() {
        // TODO Auto-generated method stub
        return productNameList2;
    }

    public List<String> getVehicleID() {
        // TODO Auto-generated method stub
        return vehicleIDList;
    }


    public void setpartCodeList(List<String> partCodeList) {
        this.partCodeList = partCodeList;
    }

    public void setPrice_List(List<String> price_List) {
        this.price_List = price_List;
    }

    public void setMrpList(List<String> mrpList) {
        this.mrpList = mrpList;
    }

    public void setCatagoryList(List<String> catagoryList) {
        this.catagoryList = catagoryList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public List<String> getpartCodeList() {
        return partCodeList;
    }

    public List<String> getCatagoryList() {
        return catagoryList;
    }

    public List<String> getPrice_List() {
        return price_List;
    }

    public List<String> getMrpList() {
        return mrpList;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setDespcriptionList(List<String> despcriptionList) {
        this.despcriptionList = despcriptionList;
    }

    public List<String> getDescriptionList() {
        return despcriptionList;
    }

    public void setAutocompletePartNoList(ArrayList<String> autocompletePartNoList) {
        this.autocompletePartNoList = autocompletePartNoList;
    }

    public void setAutocompleteDespcriptionList(ArrayList<String> autocompleteDespcriptionList) {
        this.autocompleteDespcriptionList = autocompleteDespcriptionList;
    }

    public ArrayList<String> getAutocompletepartnoList() {
        return autocompletePartNoList;
    }

    public List<String> getAutocompleteDescriptionList() {
        return autocompleteDespcriptionList;
    }

    public void setoriginalList(List<String> originalList) {
        this.originalList = originalList;
    }

    public void setequivalentList(List<String> equivalentList) {
        this.equivalentList = equivalentList;
    }

    public List<String> getoriginalList() {
        return originalList;
    }

    public List<String> getequivalentList() {
        return equivalentList;
    }

}
