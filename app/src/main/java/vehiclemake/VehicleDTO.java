package vehiclemake;

import java.util.List;

public class VehicleDTO {
    private List<String> partDespcriptionList;
    private List<String> partImageList;
    private List<String> partNoIDList;
    private List<String> partNoList;
    private List<String> productIDList;
    private List<String> productNameList;
    private List<String> repaireKitno;
    private List<String> servicepart;
    private List<String> vehicleFlagList;
    private List<String> vehicleIDList;
    private List<String> vehicleNameList;
    private List<String> vehiclePartProductNOList;
    private List<String> priceList;


    public List<String> getVehicleNameList() {
        return this.vehicleNameList;
    }

    public void setVehicleNameList(List<String> vehicleNameList) {
        this.vehicleNameList = vehicleNameList;
    }

    public List<String> getVehicleIDList() {
        return this.vehicleIDList;
    }

    public void setVehicleIDList(List<String> vehicleIDList) {
        this.vehicleIDList = vehicleIDList;
    }

    public List<String> getProductNameList() {
        return this.productNameList;
    }

    public void setProductNameList(List<String> productNameList) {
        this.productNameList = productNameList;
    }

    public List<String> getProductIDList() {
        return this.productIDList;
    }

    public void setProductIDList(List<String> productIDList) {
        this.productIDList = productIDList;
    }

    public List<String> getPartNoIDList() {
        return this.partNoIDList;
    }

    public void setPartNoIDList(List<String> partNoIDList) {
        this.partNoIDList = partNoIDList;
    }

    public List<String> getPartNoList() {
        return this.partNoList;
    }

    public void setPartNoList(List<String> partNoList) {
        this.partNoList = partNoList;
    }

    public List<String> getPartDespcriptionList() {
        return this.partDespcriptionList;
    }

    public void setPartDespcriptionList(List<String> partDespcriptionList) {
        this.partDespcriptionList = partDespcriptionList;
    }

    public List<String> getPartImageList() {
        return this.partImageList;
    }

    public void setPartImageList(List<String> partImageList) {
        this.partImageList = partImageList;
    }

    public List<String> getVehiclePartProductNOList() {
        return this.vehiclePartProductNOList;
    }

    public void setVehiclePartProductNOList(List<String> vehiclePartProductNOList) {
        this.vehiclePartProductNOList = vehiclePartProductNOList;
    }

    public List<String> getProductRepairKitNOList() {
        return this.repaireKitno;
    }

    public void setProductRepairKitNOList(List<String> repaireKitno) {
        this.repaireKitno = repaireKitno;
    }

    public List<String> getProductservicepartNOList() {
        return this.servicepart;
    }

    public void setProductservicepartNOList(List<String> servicepart) {
        this.servicepart = servicepart;
    }

    public void setFlagList(List<String> vehicleFlagList) {
        this.vehicleFlagList = vehicleFlagList;
    }

    public List<String> getFlagList() {
        return this.vehicleFlagList;
    }

    public void setPriceList(List<String> priceList) {
        this.priceList = priceList;
    }

    public List<String> getPriceList() {
        return priceList;
    }
}
