package vehiclemake;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/* compiled from: VehiclePartItemActivity_Adapter */
class VehicleMakeHolder2 {
    TextView app;
    TextView descrip;
    ImageView logo;
    ImageView logoimage;
    TextView partno;
    ImageView repair;
    LinearLayout rows;
    ImageView service;
    TextView sno;
    Button addcart;

    VehicleMakeHolder2() {
    }
}
