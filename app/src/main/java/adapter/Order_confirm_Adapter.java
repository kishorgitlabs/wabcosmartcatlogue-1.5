package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;



/*
*
 * Created by system01 on 2/12/2017.
*/


public class Order_confirm_Adapter extends ArrayAdapter<String> {

    private  ArrayList<Integer> totalList;
    private  Context context;
    private ArrayList<String> PartNumList,DescriptionList,QuantityList,partNameList;
    private ArrayList<Integer> PartIDList,PriceList;


    public Order_confirm_Adapter(Context context, ArrayList<String> partNameList, ArrayList<String> partNumList, ArrayList<String> descriptionList, ArrayList<String> quantityList, ArrayList<Integer> priceList, ArrayList<Integer> totalList) {
        super(context, R.layout.adapter_order_confirm, partNameList);

        this.context = context;
        this.partNameList = partNameList;
        this.PartNumList = partNumList;
        this.DescriptionList = descriptionList;
        this.QuantityList = quantityList;
        this.PriceList =priceList;
        this.totalList=totalList;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_order_confirm, parent, false);

            TextView sno = (TextView) convertView.findViewById(R.id.sno);
            TextView Partno = (TextView) convertView.findViewById(R.id.partno);
            TextView Part_name = (TextView) convertView.findViewById(R.id.part_name);
            TextView Partno_desc = (TextView) convertView.findViewById(R.id.partno_desc);
            TextView Part_quantity = (TextView) convertView.findViewById(R.id.part_quantity);
            TextView Part_price = (TextView) convertView.findViewById(R.id.part_price);
            TextView Part_total = (TextView) convertView.findViewById(R.id.part_total);

            sno.setText(Integer.toString(position+1)+".");
            Partno.setText(PartNumList.get(position));
            Part_name.setText(partNameList.get(position));

            Partno_desc.setText(DescriptionList.get(position));
            Part_quantity.setText(QuantityList.get(position).toString());
            Part_price.setText(context.getResources().getString(R.string.Rs)+" "+PriceList.get(position).toString()+".00");
            Part_total.setText(context.getResources().getString(R.string.Rs)+" "+totalList.get(position).toString()+".00");
            convertView.setTag(convertView);

        }

        return convertView;
    }




}
