package directory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class UNZipDownloadFile {
    private static final String FILENAME = "wabco.zip";
    private Activity activity;
    private SharedPreferences preferences;
    private ProgressDialog unZipDialog;

    private class UnZipTask extends AsyncTask<String, String, String> {
        private UnZipTask() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            UNZipDownloadFile.this.unZipDialog = new ProgressDialog(UNZipDownloadFile.this.activity);
            UNZipDownloadFile.this.unZipDialog.setMessage("Please Wait...Extracting zip file ... ");
            UNZipDownloadFile.this.unZipDialog.setProgressStyle(0);
            UNZipDownloadFile.this.unZipDialog.setCancelable(false);
            UNZipDownloadFile.this.unZipDialog.show();
        }

        protected String doInBackground(String... file) {
            try {
                UNZipDownloadFile.this.Unzip(file[0], file[1]);
                return "success";
            } catch (Exception e) {
                e.printStackTrace();
                return "unsuccess";
            }
        }

        protected void onPostExecute(String result) {
            UNZipDownloadFile.this.unZipDialog.dismiss();
            if (result.equals("success")) {
                UNZipDownloadFile.this.deleteFile();
                Toast.makeText(UNZipDownloadFile.this.activity, "Download Successfully", 0).show();
                return;
            }
            Toast.makeText(UNZipDownloadFile.this.activity, "Download Unsuccessfully", 0).show();
        }
    }

    public UNZipDownloadFile(Activity activity) {
        this.activity = activity;
        this.preferences = activity.getSharedPreferences("DOWNLOAD_CHECK", 0);
    }

    @SuppressLint({"SdCardPath"})
    public void unZipFile() {
        String checkFileStorageArea = this.preferences.getString("DOWNLOAD_FILE", null);
        String INPUT_DB_PATH;
        String OUTPUT_DB_PATH;
        if (checkFileStorageArea.equals("INTERNAL")) {
            INPUT_DB_PATH = "/data/data/" + this.activity.getPackageName() + "/" + "app_wabcodb/" + FILENAME;
            OUTPUT_DB_PATH = "/data/data/" + this.activity.getPackageName() + "/" + "app_wabcodb/";
            new UnZipTask().execute(new String[]{INPUT_DB_PATH, OUTPUT_DB_PATH});
        } else if (checkFileStorageArea.equals("EXTERNAL")) {
            INPUT_DB_PATH = Environment.getExternalStorageDirectory() + "/wabcodb/" + FILENAME.toString();
            OUTPUT_DB_PATH = Environment.getExternalStorageDirectory() + "/wabcodb/".toString();
            new UnZipTask().execute(new String[]{INPUT_DB_PATH, OUTPUT_DB_PATH});
        }
    }

    public boolean Unzip(String inputZipFile, String destinationDirectory) throws IOException {
        List<String> zipFiles = new ArrayList();
        File file = new File(inputZipFile);
        file = new File(destinationDirectory);
        file.mkdir();
        ZipFile zipFile = new ZipFile(file, 1);
        Enumeration zipFileEntries = zipFile.entries();
        while (zipFileEntries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
            String currentEntry = entry.getName();
            File destFile = new File(file, currentEntry);
            if (currentEntry.endsWith(".zip")) {
                zipFiles.add(destFile.getAbsolutePath());
            }
            destFile.getParentFile().mkdirs();
            try {
                if (!entry.isDirectory()) {
                    BufferedInputStream is = new BufferedInputStream(zipFile.getInputStream(entry));
                    byte[] data = new byte[AccessibilityNodeInfoCompat.ACTION_PREVIOUS_HTML_ELEMENT];
                    BufferedOutputStream dest = new BufferedOutputStream(new FileOutputStream(destFile), AccessibilityNodeInfoCompat.ACTION_PREVIOUS_HTML_ELEMENT);
                    while (true) {
                        int currentByte = is.read(data, 0, AccessibilityNodeInfoCompat.ACTION_PREVIOUS_HTML_ELEMENT);
                        if (currentByte == -1) {
                            break;
                        }
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        zipFile.close();
        for (String zipName : zipFiles) {
            Unzip(zipName, new StringBuilder(String.valueOf(destinationDirectory)).append(File.separatorChar).append(zipName.substring(0, zipName.lastIndexOf(".zip"))).toString());
        }
        return true;
    	
    }

    public void deleteFile() {
        String checkFileStorageArea = this.preferences.getString("DOWNLOAD_FILE", null);
        if (checkFileStorageArea.equals("INTERNAL")) {
            new File(this.activity.getDir("wabcodb", 0), FILENAME).delete();
        } else if (checkFileStorageArea.equals("EXTERNAL")) {
            new File(new File(Environment.getExternalStorageDirectory(), FILENAME), FILENAME).delete();
        }
    }
}
