package directory;

import android.os.Environment;
import android.os.StatFs;

public class CheckInternalMemory {
    public double getInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((double) ((((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize()))
                / 7)) / 1024.0d;
    }
}
